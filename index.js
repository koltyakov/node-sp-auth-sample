const { AuthConfig } = require('node-sp-auth-config');
const { PnpNode } = require('sp-pnp-node');
const { sp, Web } = require('@pnp/sp');

new AuthConfig().getContext().then(context => {

  sp.setup({
    sp: {
      fetchClientFactory: () => new PnpNode(context),
      headers: {
        accept: 'application/json;odata=verbose'
      }
    }
  });

  const web = new Web(context.siteUrl);
  return web.select('Title').get().then(({ Title }) => console.log(Title));

}).catch(console.log);
